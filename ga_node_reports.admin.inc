<?php

/**
 * @file
 * Admin Google Analytics Node Reports settings form.
 */

/**
 * Custom configuration form for the Google analytics node report.
 */
function ga_node_reports_admin_config() {
  $form = array();
  $account = google_analytics_reports_api_gafeed();

  // There are no profiles, and we should just leave it at setup.
  if (!$account) {
    $setup_help = t('Setup the Google analytics account in website to access the reports. Please <a href="/admin/config/system/google-analytics-reports-api">click here</a> to setup');
    $form['setup'] = array(
      '#type' => 'markup',
      '#title' => t('Initial setup'),
      '#markup' => $setup_help,
    );
  }
  elseif ($account->isAuthenticated()) {
    // List the content type names.
    $content_types = array();
    $node_types = node_type_get_types();
    foreach ($node_types as $key => $value) {
      $content_types[$key] = $value->name;
    }
    $form['ga_node_reports_content_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Google analytics report content types'),
      '#description' => t('Select the content types that need to show the analytics reports.'),
      '#options' => $content_types,
      '#default_value' => variable_get('ga_node_reports_content_types', array()),
    );
    return system_settings_form($form);
  }
  return $form;
}
