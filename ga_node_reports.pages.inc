<?php

/**
 * @file
 * Node wise Google analytics report.
 */

/**
 * Custom function to get the node wise report.
 */
function ga_node_reports_view_node_report($node) {
  $nid = $node->nid;
  // Get the node URL.
  $nodeurl = url('node/' . $nid);
  $output = '';
  // Filter form.
  $form = drupal_get_form('ga_node_reports_analytics_form', $nid);
  $output .= drupal_render($form);
  // Constructing the Analytics feed parameter.
  $start_date = isset($_GET['start_date']) ? strtotime($_GET['start_date']) : strtotime('-6 days');
  $end_date = isset($_GET['end_date']) ? strtotime($_GET['end_date']) : strtotime('now');

  $settings = array();

  // Pageviews Analytics report.
  $params = array(
    'dimensions' => array('ga:date'),
    'metrics' => array('ga:pageviews', 'ga:sessions'),
    'start_date' => $start_date,
    'end_date' => $end_date,
    'sort_metric' => array('ga:date'),
    'filters' => 'ga:pagePath=~' . $nodeurl,
  );
  $data = google_analytics_reports_api_report_data($params);
  $results = $data->results;

  if (is_object($results) && !isset($results->error)) {

    // Chart details building.
    $chart_date = $chart_page = $chart_session = array();

    foreach ($results->rows as $records) {
      $chart_date[] = date('d-m-y', $records['date']);
      $chart_page[] = $records['pageviews'];
      $chart_session[] = $records['sessions'];
    }

    // Google chart integration.
    $chart_header = array('Pageviews', 'Sessions');
    $chart_rows = array($chart_page, $chart_session);
    $settings['chart']['pageview_session'] = array(
      'header' => $chart_date,
      'rows' => $chart_rows,
      'columns' => $chart_header,
      'chartType' => GOOGLE_CHART_TOOLS_DEFAULT_CHART,
      'containerId' => 'pageview_session',
      'options' => array(
        'curveType' => "function",
        'forceIFrame' => FALSE,
        'title' => 'Pageviews and Sessions',
        'height' => 350,
      ),
    );
    $output .= '<div id="pageview_session"></div>';
  }
  // EOF Pageviews.
  // Pageviews Traffic source  report.
  $params = array(
    'dimensions' => array('ga:source'),
    'metrics' => array('ga:pageviews'),
    'start_date' => $start_date,
    'end_date' => $end_date,
    'sort_metric' => array('ga:source'),
    'filters' => 'ga:pagePath=~' . $nodeurl,
  );
  $data = google_analytics_reports_api_report_data($params);
  $results = $data->results;
  if (is_object($results) && !isset($results->error)) {
    // Chart details building.
    $chart_page = $chart_source = array();

    foreach ($results->rows as $records) {
      $chart_source[] = $records['source'];
      $chart_page[] = $records['pageviews'];
    }

    // Google chart integration.
    $chart_header = array('Pageviews');
    $chart_rows = array($chart_page);
    $settings['chart']['traffic_source'] = array(
      'header' => $chart_source,
      'rows' => $chart_rows,
      'columns' => $chart_header,
      'chartType' => 'PieChart',
      'containerId' => 'traffic_source',
      'options' => array(
        'is3D' => TRUE,
        'title' => 'Traffic source',
        'height' => 350,
      ),
    );
    $output .= '<div id="traffic_source"></div>';
  }
  // EOF Traffic source.
  // Geo location report.
  $params = array(
    'dimensions' => array('ga:country'),
    'metrics' => array('ga:pageviews'),
    'start_date' => $start_date,
    'end_date' => $end_date,
    'sort_metric' => array('ga:pageviews'),
    'filters' => 'ga:pagePath=~' . $nodeurl,
  );
  $data = google_analytics_reports_api_report_data($params);
  $results = $data->results;
  if (is_object($results) && !isset($results->error)) {
    // Chart details building.
    $chart_page = $chart_country = array();
    foreach ($results->rows as $records) {
      $chart_country[] = $records['country'];
      $chart_page[] = $records['pageviews'];
    }

    // Google chart integration.
    $chart_header = array('Pageviews');
    $chart_rows = array($chart_page);
    $settings['chart']['geolocation'] = array(
      'header' => $chart_country,
      'rows' => $chart_rows,
      'columns' => $chart_header,
      'chartType' => 'GeoChart',
      'containerId' => 'geolocation',
      'options' => array(
        'title' => 'Users from country',
        'backgroundColor' => '#B1E7F2',
        'displayMode' => 'region',
        'colorAxis' => array('colors' => ['#e7711c', '#4374e0']),
      ),
    );
    $output .= '<div id="geolocation"></div>';
  }
  // EOF Geo location.
  // Draw it.
  if (count($settings)) {
    draw_chart($settings);
  }

  return $output;
}

/**
 * Custom form for Google analytics report filter.
 */
function ga_node_reports_analytics_form($form, $form_state, $nid) {
  $start_date = isset($_GET['start_date']) ? strtotime($_GET['start_date']) : strtotime('-6 days');
  $end_date = isset($_GET['end_date']) ? strtotime($_GET['end_date']) : strtotime('now');

  $form = array();

  $form['#attached']['css'][] = drupal_get_path('module', 'ga_node_reports') . '/ga_node_reports.css';

  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $nid,
  );
  $form['start_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Start date'),
    '#default_value' => date('d-m-Y', $start_date),
    '#description' => t('Ex: @date', array("@date" => date('d-m-Y', $start_date))),
    '#size' => 10,
  );
  $form['end_date'] = array(
    '#type' => 'textfield',
    '#title' => t('End date'),
    '#default_value' => date('d-m-Y', $end_date),
    '#description' => t('Ex: @date', array("@date" => date('d-m-Y', $end_date))),
    '#size' => 10,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Apply',
    '#prefix' => '<div class="buttons">',
  );

  $form['reset'] = array(
    '#type' => 'submit',
    '#value' => 'Reset',
    '#submit' => array('ga_node_reports_analytics_form_reset'),
    '#suffix' => '</div>',
  );
  return $form;
}

/**
 * Custom form submit handler.
 */
function ga_node_reports_analytics_form_submit($form, &$form_state) {
  $nid = $form_state['values']['nid'];
  // Building the query parameter based on the filters.
  $query_param = array(
    'start_date' => $form_state['values']['start_date'],
    'end_date' => $form_state['values']['end_date'],
  );

  // Rederting to the same page.
  drupal_goto('node/' . $nid . '/analytics', array('query' => $query_param));
}

/**
 * Custom form submit handler.
 */
function ga_node_reports_analytics_form_reset($form, &$form_state) {
  $nid = $form_state['values']['nid'];
  // Rederting to the same page.
  drupal_goto('node/' . $nid . '/analytics');
}
